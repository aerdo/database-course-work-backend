module.exports = (sequelize, dataTypes) => {
    return  sequelize.define('grades', {
            grade_id: {
                type: dataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            student_id: {
                type: dataTypes.INTEGER
            },
            plan_id: {
                type: dataTypes.INTEGER
            },
            grade: {
                type: dataTypes.STRING(1)
            },
            grade_date: {
                type: dataTypes.DATE
            }
        },
        {
            freezeTableName: true,
            timestamps: false,
        }
    );
};