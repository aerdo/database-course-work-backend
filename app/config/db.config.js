module.exports = {
    HOST: "localhost",
    USER: "postgres",
    PASSWORD: "123456",
    database: "db_coursework",
    dialect: "postgres",
    pool: {
        max: 5,
        min: 0
    }
};