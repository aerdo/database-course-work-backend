const teachers = require("../controllers/teacher.controller");

module.exports = app => {
    var router = require("express").Router();

    router.post("/teachers", teachers.createTeacher); // создать преподавателя
    router.get("/teachers", teachers.findAllTeachers); // вывести всех преподов
    router.put("/teachers/:id", teachers.updateTeacher); // обновить препода
    router.delete("/teachers/:id", teachers.deleteTeacher); // удалить препода

    app.use('/api', router);
};