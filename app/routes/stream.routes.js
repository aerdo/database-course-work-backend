const streams = require('../controllers/stream.controller');


module.exports = app => {
    var router = require("express").Router();

    router.get("/streams", streams.findAllStreams);
    router.post("/streams", streams.createStream);
    router.put("/streams/:id", streams.updateStream);
    router.delete("/streams/:id", streams.deleteStream);

    router.get("/streams/:id/classes", streams.findClassesByStreamId);
    router.post("/classes",streams.createClass);
    router.delete("/classes/:id",streams.deleteClass);

    router.get("/streams/:id/assignments", streams.findAssignmentsByStreamId);
    router.post("/assignments",streams.createAssignment);
    router.delete("/assignments/:id",streams.deleteAssignment);

    router.get("/streams/:id/assignments/spare", streams.findSpareTasksByStreamId);

    app.use('/api', router);
};