const students = require('../controllers/student.controller');


module.exports = app => {
    var router = require("express").Router();

    router.get("/students", students.findAllStudents);
    router.get("/groups/:id", students.findStudentsByGroup);

    router.post("/students", students.createStudent);
    router.put("/students/:id", students.updateStudent); // создаем без указания группы
    router.delete("/students/:id", students.deleteStudent);

    router.get("/students/spare", students.getStudentsWOGroup);

    app.use('/api', router);
};